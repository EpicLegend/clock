'use strict';
//= template/jquery.min.js
//= template/greensock.js
//= template/popper.min.js
//= template/bootstrap.min.js
//= template/bootstrap-select.min.js
//= template/jquery.fancybox.min.js
//= template/wow.min.js
//= library/owl.carousel.js
//= template/mixitup.min.js
//= template/paraxify.js
//= template/validate.js
//= template/custom.js

//= ../node_modules/lazysizes/lazysizes.js
//= library/slick.js

document.addEventListener("DOMContentLoaded", function () {
	$('#slider-owl').owlCarousel({
		loop:true,
		margin:10,
		nav:true,
		items: 1,
		dots: false,
		autoplay: true,
		smartSpeed: 2000,
		autoplayTimeout: 7000,
		autoplayHoverPause: true,
		responsive:{
			0:{
				items:1
			},
			600:{
				items:1
			},
			1000:{
				items:1
			}
		}
	})

	$('#product-single-slider').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		arrows: false,
		fade: true,
		asNavFor: '#product-single-slider-nav'
	});
	$('#product-single-slider-nav').slick({
		slidesToShow: 3,
		slidesToScroll: 1,
		asNavFor: '#product-single-slider',
		dots: true,
		centerMode: true,
		focusOnSelect: true
	});




	var CartPlusMinus = jQuery('.cart-plus-minus');
	CartPlusMinus.prepend('<div class="dec qtybutton">-</div>');
	CartPlusMinus.append('<div class="inc qtybutton">+</div>');
	jQuery(".qtybutton").on("click", function() {
		var $button = jQuery(this);
		var oldValue = $button.parent().find("input").val();
		if ($button.text() === "+") {
			var newVal = parseFloat(oldValue) + 1;
		} else {
			if (oldValue > 0) {
				var newVal = parseFloat(oldValue) - 1;
			} else {
				newVal = 0;
			}
		}
		$button.parent().find("input").val(newVal);
		inputChangeCount();
	});
	$(".cart-plus-minus .cart-plus-minus-box").change(function () {
		inputChangeCount();
	});
	function inputChangeCount() {
		console.log("chenge number");
	}








	function moveMinicart () {
		var scroll_wrapper = $('#cart-scroll');
		var scroll = scroll_wrapper.children();
		var desktop_wrapper = $('#cart-desktop');
		var desktop = desktop_wrapper.children();
		// var mobile_wrapper = $('#cart-mobile');
		// var mobile = mobile_wrapper.children();

		if ( window.innerWidth < 991 ) {
			// Вставить в мобилку
			if (mobile.lenght) return false;
				// mobile_wrapper.append(desktop.detach());
		} else {
			if (desktop.lenght) return false;
			// desktop_wrapper.append( mobile.detach() );
		}

		if ( $(window).scrollTop() > 100 ) {
			// Вставить в декстом с скролом
			if (scroll.lenght) {
				return false;
			}
			scroll_wrapper.append(desktop.detach());
		} else {
			// Вставить в декстом без скрола
			if (desktop.lenght) {
				return false;
			}
			desktop_wrapper.append( scroll.detach() );
		}
	}
	$(window).scroll(function () {
		moveMinicart();
	});
	// $(window).resize(function () {
	// 	moveMinicart();
	// });
	moveMinicart ();



});
